package com.paowaric.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        Plane plane1 = new Plane("Boeing", "Boeing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Crocodile crocodile1 = new Crocodile("Kae");
        crocodile1.Crawl();
        crocodile1.swim();
        crocodile1.eat();
        crocodile1.walk();

        Snake snake1 = new Snake("Kapa");
        snake1.Crawl();
        snake1.eat();

        Submarine submarine1 = new Submarine("U-37", "U-37 Engine");
        submarine1.swim();

        Bird bird1 = new Bird("Phoenix");
        bird1.fly();
        bird1.landing();
        bird1.eat();

        Rat rat1 = new Rat("FuFu");
        rat1.walk();
        rat1.eat();

        Cat cat1 = new Cat("Hina");
        cat1.walk();
        cat1.sleep();

        Dog dog1 = new Dog("Alice");
        dog1.run();
        dog1.eat();
        dog1.sleep();

        Human human1 = new Human("Koharu");
        human1.walk();
        human1.eat();
        human1.sleep();

        System.out.println("\n----------\n");
        Flyable[] flyablesObjects = { bat1, plane1, bird1 };
        for (int i = 0; i < flyablesObjects.length; i++) {
            flyablesObjects[i].takeoff();
            flyablesObjects[i].fly();
            flyablesObjects[i].landing();
        }
        Swimable[] swimables = {crocodile1,submarine1,fish1};
        for (int i = 0; i < flyablesObjects.length; i++) {
            swimables[i].swim();
        }
        Walkable[] walkables = {rat1,human1,dog1,cat1};
        for (int i = 0; i < flyablesObjects.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }
        Crawlable[] crawlables = {snake1, crocodile1};
        for (int i = 0; i < flyablesObjects.length; i++) {
            crawlables[i].Crawl();
        }
    }
}
